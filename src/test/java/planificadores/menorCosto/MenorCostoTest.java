package planificadores.menorCosto;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import domain.Alojamiento;
import domain.Atraccion;
import domain.Ciudad;
import domain.Mapa;
import domain.Money;
import domain.Ruta;

public class MenorCostoTest {

    // Criterios de aceptación
    @Test(expected = NullPointerException.class)
    public void caso1() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad = new Ciudad("ciudad");
        mapa.addCiudad(ciudad);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(ciudad.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void caso2() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad = new Ciudad("ciudad");
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, ciudad.getNombre());
    }

    @Test(expected = Exception.class)
    public void caso3() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("ciudad 1");
        Ciudad ciudad2 = new Ciudad("ciudad 2");
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(ciudad1.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, ciudad1.getNombre());
        mc.elegirAlojamiento(ciudad2.getNombre());
    }

    @Test
    public void caso4() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("ciudad 1");
        Alojamiento a1 = new Alojamiento("a1", new Money("99"));
        Alojamiento a2 = new Alojamiento("a2", new Money("80"));
        Ciudad ciudad2 = new Ciudad("ciudad 2");
        ciudad1.addAlojamiento(a1);
        ciudad1.addAlojamiento(a2);
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(ciudad1.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, ciudad1.getNombre());
        assertEquals(a2.getNombre(), mc.elegirAlojamiento(ciudad1.getNombre()));
    }

    @Test(expected = Exception.class)
    public void caso5() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("ciudad 1");
        Ciudad ciudad2 = new Ciudad("ciudad 2");
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(ciudad1.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, ciudad1.getNombre());
        mc.elegirAtraccion(ciudad2.getNombre());
    }

    @Test
    public void caso6() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("ciudad 1");
        Atraccion a1 = new Atraccion("atraccion 1", new Money("76"));
        Atraccion a2 = new Atraccion("atraccion 2", new Money("120"));
        Ciudad ciudad2 = new Ciudad("ciudad 2");
        ciudad1.addAtraccion(a1);
        ciudad1.addAtraccion(a2);
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(ciudad1.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, ciudad1.getNombre());
        assertEquals(a1.getNombre(), mc.elegirAtraccion(ciudad1.getNombre()));
    }

    @Test(expected = Exception.class)
    public void caso7() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("ciudad 1");
        Ciudad ciudad2 = new Ciudad("ciudad 2");
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(ciudad1.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, ciudad1.getNombre());
        mc.elegirRuta(ciudad1.getNombre(), ciudad2.getNombre());
    }

    @Test
    public void caso8() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("ciudad 1");
        Ciudad ciudad2 = new Ciudad("ciudad 2");
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        Ruta r1 = new Ruta(ciudad1, ciudad2, 6);
        Ruta r2 = new Ruta(ciudad2, ciudad1, 4);
        r1.setNombre("r1");
        r2.setNombre("r2");
        r1.setCosto(new Money("52"));
        r2.setCosto(new Money("51"));
        mapa.addRuta(r1);
        mapa.addRuta(r2);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(ciudad1.getNombre());
        ciudadesElegidas.add(ciudad2.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, ciudad1.getNombre());
        String ret = mc.elegirRuta(ciudad1.getNombre(), ciudad2.getNombre());
        assertEquals(r2.getNombre(), ret);
    }

    @Test
    public void caso9() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad = new Ciudad("ciudad");
        mapa.addCiudad(ciudad);
        Collection<String> ciudadElegidas = new ArrayList<String>();
        ciudadElegidas.add(ciudad.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadElegidas, ciudad.getNombre());
        assertEquals(1, mc.getItinerario().size());
        assertEquals(ciudad.getNombre(), mc.getItinerario().get(0));
    }

    @Test
    public void caso10() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("A");
        Ciudad ciudad2 = new Ciudad("B");
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        Collection<String> ciudadElegidas = new ArrayList<String>();
        ciudadElegidas.add(ciudad1.getNombre());
        ciudadElegidas.add(ciudad2.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadElegidas, ciudad1.getNombre());
        assertEquals(1, mc.getItinerario().size());
        assertEquals(ciudad1.getNombre(), mc.getItinerario().get(0));
    }

    @Test
    public void caso11() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("A");
        Ciudad ciudad2 = new Ciudad("B");
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        Ruta r = new Ruta(ciudad1, ciudad2, 4);
        r.setCosto(new Money("20"));
        mapa.addRuta(r);
        Collection<String> ciudadElegidas = new ArrayList<String>();
        ciudadElegidas.add(ciudad1.getNombre());
        ciudadElegidas.add(ciudad2.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadElegidas, ciudad1.getNombre());
        assertEquals(2, mc.getItinerario().size());
        assertEquals(ciudad1.getNombre(), mc.getItinerario().get(0));
        assertEquals(ciudad2.getNombre(), mc.getItinerario().get(1));
    }

    @Test
    public void caso12() {
        Mapa mapa = Mapa.getMapa();
        Ciudad ciudad1 = new Ciudad("A");
        Ciudad ciudad2 = new Ciudad("B");
        Ciudad ciudad3 = new Ciudad("C");
        Ruta   ruta1   = new Ruta(ciudad1, ciudad2, 4);
        Ruta   ruta2   = new Ruta(ciudad1, ciudad3, 4);
        ruta1.setCosto(new Money("3"));
        ruta2.setCosto(new Money("4"));
        mapa.addCiudad(ciudad1);
        mapa.addCiudad(ciudad2);
        mapa.addCiudad(ciudad3);
        mapa.addRuta(ruta1);
        mapa.addRuta(ruta2);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(ciudad1.getNombre());
        ciudadesElegidas.add(ciudad2.getNombre());
        ciudadesElegidas.add(ciudad3.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, ciudad1.getNombre());
        assertEquals(4, mc.getItinerario().size());
        assertEquals(ciudad1.getNombre(), mc.getItinerario().get(0));
        assertEquals(ciudad2.getNombre(), mc.getItinerario().get(1));
        assertEquals(ciudad1.getNombre(), mc.getItinerario().get(2));
        assertEquals(ciudad3.getNombre(), mc.getItinerario().get(3));
    }

    @Test
    public void caso13() {
        Mapa mapa = Mapa.getMapa();
        Ciudad a = new Ciudad("A");
        Ciudad b = new Ciudad("B");
        Ciudad c = new Ciudad("C");
        Ciudad d = new Ciudad("D");
        Ciudad e = new Ciudad("E");
        Ruta ruta1 = new Ruta(a, c, 4);
        Ruta ruta2 = new Ruta(a, b, 4);
        Ruta ruta3 = new Ruta(b, d, 6);
        Ruta ruta4 = new Ruta(b, e, 5);
        Ruta ruta5 = new Ruta(d, e, 7);
        ruta1.setCosto(new Money("4"));
        ruta2.setCosto(new Money("3"));
        ruta3.setCosto(new Money("2"));
        ruta4.setCosto(new Money("1"));
        ruta5.setCosto(new Money("2"));
        mapa.addCiudad(a);
        mapa.addCiudad(b);
        mapa.addCiudad(c);
        mapa.addCiudad(d);
        mapa.addCiudad(e);
        mapa.addRuta(ruta1);
        mapa.addRuta(ruta2);
        mapa.addRuta(ruta3);
        mapa.addRuta(ruta4);
        mapa.addRuta(ruta5);
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(a.getNombre());
        ciudadesElegidas.add(b.getNombre());
        ciudadesElegidas.add(c.getNombre());
        ciudadesElegidas.add(d.getNombre());
        ciudadesElegidas.add(e.getNombre());
        MenorCosto mc = new MenorCosto();
        mc.planificarViaje(mapa, ciudadesElegidas, a.getNombre());
        assertEquals(6, mc.getItinerario().size());
        assertEquals(a.getNombre(), mc.getItinerario().get(0));
        assertEquals(c.getNombre(), mc.getItinerario().get(1));
        assertEquals(a.getNombre(), mc.getItinerario().get(2));
        assertEquals(b.getNombre(), mc.getItinerario().get(3));
        assertEquals(d.getNombre(), mc.getItinerario().get(4));
        assertEquals(e.getNombre(), mc.getItinerario().get(5));
        assertEquals(new Money("15"), mc.getCostoDelItinerario());
    }

    @Test
    public void caso14() {
        Ciudad ciudad = new Ciudad("Buenos Aires");
        ServicioPreciosPromedios spp = ServicioPreciosPromedios.getInstancia();
        assertEquals(Money.MONTO_DESCONOCIDO, spp.getPrecioPromedioAlojamientos(ciudad));
        assertEquals(Money.MONTO_DESCONOCIDO, spp.getPrecioPromedioAtracciones(ciudad));
    }

    @Test
    public void caso15() {
        Ciudad ciudad = new Ciudad("Buenos Aires");
        System.setProperty("precios.promedio.impl", "paquete.Clase");
        ServicioPreciosPromedios.cargarInstancia();
        ServicioPreciosPromedios spp = ServicioPreciosPromedios.getInstancia();
        assertEquals(Money.MONTO_DESCONOCIDO, spp.getPrecioPromedioAlojamientos(ciudad));
        assertEquals(Money.MONTO_DESCONOCIDO, spp.getPrecioPromedioAtracciones(ciudad));
    }

    @Test
    public void caso16() {
        Ciudad ciudad = new Ciudad("Buenos Aires");
        System.setProperty("precios.promedio.impl", "planificadores.menorCosto.PreciosPromediosGateway");
        ServicioPreciosPromedios.cargarInstancia();
        ServicioPreciosPromedios spp = ServicioPreciosPromedios.getInstancia();
        assertNotEquals(Money.MONTO_DESCONOCIDO, spp.getPrecioPromedioAlojamientos(ciudad));
        assertNotEquals(Money.MONTO_DESCONOCIDO, spp.getPrecioPromedioAtracciones(ciudad));
        assertTrue(Money.CERO.compareTo(spp.getPrecioPromedioAlojamientos(ciudad)) < 0);
        assertTrue(Money.CERO.compareTo(spp.getPrecioPromedioAtracciones(ciudad)) < 0);
    }

    // Otros test
    @Test
    public void nombre_test() {
        assertEquals("Planificador menor costo", new MenorCosto().getNombre());
    }
    
    @Test
    public void costoPromedioAlojamientosDelItinerarioTest() {
        System.setProperty("precios.promedio.impl", "planificadores.menorCosto.PreciosPromediosGateway");
        ServicioPreciosPromedios.cargarInstancia();
        Mapa mapa = Mapa.getMapa();
        Ciudad a = new Ciudad("A");
        Ciudad b = new Ciudad("B");
        Alojamiento alojamiento1 = new Alojamiento("alojamiento 1", new Money("10"));
        Alojamiento alojamiento2 = new Alojamiento("alojamiento 2", new Money("10"));
        Alojamiento alojamiento3 = new Alojamiento("alojamiento 3", new Money("10"));
        Alojamiento alojamiento4 = new Alojamiento("alojamiento 4", new Money("10"));
        a.addAlojamiento(alojamiento1);
        a.addAlojamiento(alojamiento2);
        b.addAlojamiento(alojamiento3);
        b.addAlojamiento(alojamiento4);
        Atraccion atraccion1 = new Atraccion("atraccion 1", new Money("11"));
        Atraccion atraccion2 = new Atraccion("atraccion 2", new Money("11"));
        a.addAtraccion(atraccion1);
        b.addAtraccion(atraccion2);
        mapa.addCiudad(a);
        mapa.addCiudad(b);
        Ruta r = new Ruta(a, b, 2);
        r.setCosto(new Money("12"));
        mapa.addRuta(r);
        MenorCosto mc = new MenorCosto();
        Collection<String> ciudadesElegidas = new ArrayList<String>();
        ciudadesElegidas.add(a.getNombre());
        ciudadesElegidas.add(b.getNombre());
        mc.planificarViaje(mapa, ciudadesElegidas, a.getNombre());
        ServicioPreciosPromedios spp = ServicioPreciosPromedios.getInstancia();
        Money promedioAlojA = spp.getPrecioPromedioAlojamientos(a);
        Money promedioAlojB = spp.getPrecioPromedioAlojamientos(b);
        Money promedioAtraA = spp.getPrecioPromedioAtracciones(a);
        Money promedioAtraB = spp.getPrecioPromedioAtracciones(b);
        assertEquals(promedioAlojA.sumar(promedioAlojB), mc.getCostoPromedioAlojamientosDelItinerario());
        assertEquals(promedioAtraA.sumar(promedioAtraB), mc.getCostoPromedioAtraccionesDelItinerario());
    }
}
