package planificadores.menorCosto;

import domain.Ciudad;
import domain.Money;

abstract class ServicioPreciosPromedios {

    private static ServicioPreciosPromedios INSTANCIA = null;

    public static ServicioPreciosPromedios getInstancia() {
        if (INSTANCIA == null) {
            cargarInstancia();
        }
        return INSTANCIA;
    }

    static void cargarInstancia() {
        try {
            INSTANCIA =
                    (ServicioPreciosPromedios) 
                    Class.forName(System.getProperty("precios.promedio.impl"))
                    .newInstance();
        } catch(Exception e) {
            INSTANCIA = new ServicioPreciosNoEncontrado();
        }
    }

    abstract Money getPrecioPromedioAlojamientos(Ciudad ciudad);
    abstract Money getPrecioPromedioAtracciones(Ciudad ciudad);
}
