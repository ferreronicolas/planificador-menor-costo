package planificadores.menorCosto;

import domain.Ciudad;
import domain.Money;
import precios.promedio.api.Api;

class PreciosPromediosGateway extends ServicioPreciosPromedios {

    private Api API;
    private String separador;
    private static final String msgAlojamiento = "ALOJAMIENTO";
    private static final String msgAtraccion   = "ATRACCION";

    public PreciosPromediosGateway() {
        API = new Api();
        separador = API.get("separador");
    }
    @Override
    Money getPrecioPromedioAlojamientos(Ciudad ciudad) {
        String respuesta = API.get(msgAlojamiento + separador + ciudad.getNombre());
        return new Money(respuesta);
    }

    @Override
    Money getPrecioPromedioAtracciones(Ciudad ciudad) {
        String respuesta = API.get(msgAtraccion + separador + ciudad.getNombre());
        return new Money(respuesta);
    }

}
