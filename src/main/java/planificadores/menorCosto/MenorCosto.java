package planificadores.menorCosto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import domain.Alojamiento;
import domain.Atraccion;
import domain.Ciudad;
import domain.Mapa;
import domain.Money;
import domain.Ruta;
import planificadores.PlanificadorAbstracto;

public class MenorCosto extends PlanificadorAbstracto {

    private Mapa mapa;
    private List<Ciudad> itinerario;
    private Money costoDelItinerario;
    private Collection<Ciudad> ciudadesElegidas;
    private Ciudad ciudadInicio;
    private boolean mayorAMenor;

    public MenorCosto() {
        mapa = null;
        ciudadesElegidas = new ArrayList<Ciudad>();
        itinerario       = new ArrayList<Ciudad>();
    }

    @Override
    public void planificarViaje(Mapa mapa, Collection<String> ciudades, String inicio) {
        if (mapa == null || ciudades == null || inicio == null) {
            throw new NullPointerException("los parámetros no pueden ser null.");
        }
        if (!ciudades.contains(inicio)) {
            throw new IllegalArgumentException("La ciudad de inicio debe estar incluida en las ciudades elegidas");
        }
        if (ciudades.size() != 0) {
            this.mapa = mapa;
            mapCiudades(mapa, ciudades);
            this.ciudadInicio = mapa.getCiudad(inicio);
            buscarItinerario(mapa);
        }
    }

    /*
     * Se guardan las ciudades elegidas por el usuarios en una colección.
     * Se mapean a objeto Ciudad.
     */
    private void mapCiudades(Mapa mapa, Collection<String> ciudades) {
        ciudades.stream()
                .map(mapa::getCiudad)
                .forEach(ciudadesElegidas::add);
    }

    /*
     * Para calcular un itinerario con costo bajo se empieza desde
     * la ciudad inicio elegida por el usuario y se hace DFS para
     * cada una de las ciudades vecinas. Con esto se arman varias
     * soluciones. Nos quedamos con la solución que represente el
     * menor costo.
     */
    private void buscarItinerario(Mapa mapa) {
        List<List<Ciudad>> resultados = new ArrayList<List<Ciudad>>();
        mayorAMenor = false;
        for (int i = 0; i < 2; i++) {
            List<Ciudad> itinerarioActual = new ArrayList<Ciudad>();
            DFS(mapa, ciudadInicio, itinerarioActual);
            resultados.add(itinerarioActual);
            mayorAMenor = true;
        }
        evaluarResultados(mapa, resultados);
    }

    /*
     * DFS: depth-first search
     * Algoritmo recursivo para encontrar un recorrido que pase por
     * todos los vértices de un grafo.
     * El segundo if se agrega para que el recorrido tenga en cuenta
     * el camino de vuelta cuando se llega a un vértice sin salida.
     */
    private void DFS(Mapa mapa, Ciudad ciudad, List<Ciudad> itinerario) {
        itinerario.add(ciudad);
        Comparator<Ciudad> comparador = getComparador(mapa, ciudad);
        for (Ciudad vecino : mapa.getVecinosOrdenados(ciudad, comparador)) {
            if (!itinerario.contains(vecino)) {
                DFS(mapa, vecino, itinerario);
                if (!itinerario.containsAll(ciudadesElegidas)) {
                    itinerario.add(ciudad);
                }
            }
        }
    }

    private Comparator<Ciudad> getComparador(Mapa mapa, Ciudad ciudad) {
        if (mayorAMenor) {
            return mapa.getComparatorVecinosCostoMayorAMenor(ciudad);
        }
        return mapa.getComparatorVecinosCostoMenorAMayor(ciudad);
    }

    /*
     * Se elige el itinerario con menor costo de los resultados obtenidos
     * por el método buscarItinerario. Se guarda el menos costoso y el costo
     * del mismo en las variables de instancia.
     */
    private void evaluarResultados(Mapa mapa, List<List<Ciudad>> resultados) {
        List<Ciudad> mejorItinerario = itinerario;
        Money mejorCosto = Money.MAX;
        for (List<Ciudad> itinerarioActual : resultados) {
            Money costoActual = Money.CERO;
            for (int i = 0; i < itinerarioActual.size()-1; i++) {
                Ruta rutaMasBarata = mapa.getRutaMasBarata(itinerarioActual.get(i), itinerarioActual.get(i+1));
                costoActual = costoActual.sumar(rutaMasBarata.getCosto());
            }
            if (costoActual.compareTo(mejorCosto) < 0) {
                mejorItinerario = itinerarioActual;
                mejorCosto      = costoActual;
            }
        }
        itinerario         = mejorItinerario;
        costoDelItinerario = mejorCosto;
    }

    /*
     * Devuelve el nombre del alojamiento más barato de la ciudad
     */
    @Override
    public String elegirAlojamiento(String ciudad) {
        return elegirAlojamientoMasBarato(buscarCiudad(ciudad)).getNombre();
    }

    private Alojamiento elegirAlojamientoMasBarato(Ciudad ciudad) {
        return ciudad.getAlojamientos().stream()
                .min((a1, a2) -> a1.getCostoPorDia().compareTo(a2.getCostoPorDia()))
                .get();
    }

    /*
     * Devuelve el nombre de la atracción más barata de la ciudad
     */
    @Override
    public String elegirAtraccion(String ciudad) {
        return elegirAtraccionMasBarata(buscarCiudad(ciudad)).getNombre();
    }

    private Atraccion elegirAtraccionMasBarata(Ciudad ciudad) {
        return ciudad.getAtracciones().stream()
                .min((a1, a2) -> a1.getCostoDeEntrada().compareTo(a2.getCostoDeEntrada()))
                .get();
    }

    /*
     * Devuelve el nombre de la ruta más barata que conecta dos ciudades
     */
    @Override
    public String elegirRuta(String desde, String hacia) {
        Ciudad ciudad1 = buscarCiudad(desde);
        Ciudad ciudad2 = buscarCiudad(hacia);
        return mapa.getRutaMasBarata(ciudad1, ciudad2).getNombre();
    }

    /*
     * Arroja NoSuchElementException si no se encuentra la ciudad, es decir,
     * si no está en ciudadesElegidas.
     */
    private Ciudad buscarCiudad(String nombre) {
        return ciudadesElegidas.stream()
                .filter(c -> c.getNombre().equals(nombre))
                .findFirst()
                .get();
    }

    /*
     * Devuelve el nombre del planificador en 
     * formato entendible para el usuario.
     */
    @Override
    public String getNombre() {
        return "Planificador menor costo";
    }

    /*
     * Devuelve el itinerario encontrado en formato de lista.
     */
    @Override
    public List<String> getItinerario() {
        return itinerario.stream()
                         .map(Ciudad::getNombre)
                         .collect(Collectors.toList());
    }

    public Money getCostoDelItinerario() {
        return costoDelItinerario;
    }

    /*
     * Calcula el costo promedio que se gastaría en alojamientos en cada ciudad
     * que se visita usando un servivio externo.
     */
    public Money getCostoPromedioAlojamientosDelItinerario() {
        Money costoActual = Money.CERO;
        ServicioPreciosPromedios servicio = ServicioPreciosPromedios.getInstancia();
        for (Ciudad ciudad : itinerario) {
            costoActual = costoActual.sumar(servicio.getPrecioPromedioAlojamientos(ciudad));
        }
        return costoActual;
    }

    /*
     * Calcula el costo promedio que se gastaría en atracciones en cada ciudad
     * que se visita usando un servicio externo.
     */
    public Money getCostoPromedioAtraccionesDelItinerario() {
        Money costoActual = Money.CERO;
        ServicioPreciosPromedios servicio = ServicioPreciosPromedios.getInstancia();
        for (Ciudad ciudad : itinerario) {
            costoActual = costoActual.sumar(servicio.getPrecioPromedioAtracciones(ciudad));
        }
        return costoActual;
    }
}
