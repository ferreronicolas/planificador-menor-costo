package planificadores.menorCosto;

import domain.Ciudad;
import domain.Money;

class ServicioPreciosNoEncontrado extends ServicioPreciosPromedios {

    @Override
    public Money getPrecioPromedioAlojamientos(Ciudad ciudad) {
        return Money.MONTO_DESCONOCIDO;
    }

    @Override
    public Money getPrecioPromedioAtracciones(Ciudad ciudad) {
        return Money.MONTO_DESCONOCIDO;
    }

}
